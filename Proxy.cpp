#pragma once
#include "Proxy.h"
#include "UNIT.h"

Proxy::Proxy(Unit* un) {
	unit = un;
}
bool Proxy::be_attacked(int pow) {
	if (unit->get_health() > 0) {
		return unit->be_attacked(pow);
	}
	cout << "Army died" << endl;
	return false;
}
int Proxy::get_power() {
	if (unit->get_health() > 0) {
		return unit->get_power();
	}
	return 0;
}
int Proxy::get_health() {
	return unit->get_health();
}
bool Proxy::get_flag() {
	if (unit->get_health() > 0) {
		return unit->get_power();
	}
	return true;
}
void Proxy::pr() {
	if (unit->get_health() > 0) {
		return unit->pr();
	}
}

void Proxy::inc_health(int val) {
	if (unit->get_health() > 0) {
		unit->inc_health(val);
	}
}
