#pragma once
#include "Unit.h"
#include "Composite.h"
#include <vector>

class ArmyFactory {
public:
	virtual Strong* createStrong() = 0;
	virtual Health* createHealth() = 0;
	virtual Magic* createMagic() = 0;
};


class DragonArmyFactory : public ArmyFactory {
public:
	Strong* createStrong();
	Health* createHealth();
	Magic* createMagic();
};

class KnightArmyFactory : public ArmyFactory {
public:
	Strong* createStrong();
	Health* createHealth();
	Magic* createMagic();
};

class Army {
public:
	~Army();
	void pr();
	vector<Unit*> army;
};


class Game {
public:
	Game(ArmyFactory* factory);
	CompositeUnit* createArmy(int s, int h, int m);
	Unit * createUnit(int i);
private:
	ArmyFactory* fact;
};
