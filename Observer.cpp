#pragma once
#include "Observer.h"
#include <iostream>
#include <unordered_set>

using namespace std;

void Observer::subscribe(Unit* un) {
	if (buffer.find(un) == buffer.end()) {
		buffer.insert(un);
	}
}
void Observer::unsubscribe(Unit* un) {
	if (buffer.find(un) != buffer.end()) {
		buffer.erase(un);
	}
}
void Observer::notify(int val) {
	for (auto i = buffer.begin(); i != buffer.end(); i++) {
		(*i)->inc_health(val);
	}
	buffer.clear();
}
void Observer::get_mana(int val) {
	notify(val);
}
