#pragma once
#include "Unit.h"
#include <vector>

class CompositeUnit : public Unit {

public:

	void pr() override;

	void addUnit(Unit* p) ;

	~CompositeUnit();

	bool be_attacked(int pow) override;

	int get_power() override;

	int get_health() override;

	bool get_flag() override;

	void inc_health(int val) override;

private:
	vector<Unit*> buffer;

};
