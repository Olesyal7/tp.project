#pragma once
#include <ctime>
#include <vector>
#include "Army.h"
#include "Composite.h"
#include "Proxy.h"



Strong* DragonArmyFactory::createStrong() {
	return new StrongDragon;
}
Health* DragonArmyFactory::createHealth() {
	return new HealthDragon;
}
Magic* DragonArmyFactory::createMagic() {
	return new MagicDragon;
}


Strong* KnightArmyFactory::createStrong() {
	return new StrongKnight;
}
Health* KnightArmyFactory::createHealth() {
	return new HealthKnight;
}
Magic* KnightArmyFactory::createMagic() {
	return new MagicKnight;
}


Army::~Army() {
	for (int i = 0; i < army.size(); i++) {
		delete army[i];
	}
}
void Army::pr() {
	for (int i = 0; i < army.size(); i++) {
		army[i]->pr();
	}
}

Game::Game(ArmyFactory* factory) {
	fact = factory;
}

CompositeUnit* Game::createArmy(int s, int h, int m) {
	CompositeUnit* p = new CompositeUnit();
	for (int i = 0; i < s; i++) {
		Unit* un = fact->createStrong();
		Proxy * unit = new Proxy(un);
		p->addUnit(unit);
	}
	for (int i = 0; i < h; i++) {
		Unit* un = fact->createHealth();
		Proxy * unit = new Proxy(un);
		p->addUnit(unit);
	}
	for (int i = 0; i < m; i++) {
		Unit* un = fact->createMagic();
		Proxy * unit = new Proxy(un);
		p->addUnit(unit);
	}
	return p;
}

Unit * Game::createUnit(int i) {
	if (i == 0) {
		Unit* un = fact->createStrong();
		Proxy * unit = new Proxy(un);
		return unit;
	}
	else if (i == 1) {
		Unit* un = fact->createHealth();
		Proxy * unit = new Proxy(un);
		return unit;
	}
	else {
		Unit* un = fact->createHealth();
		Proxy * unit = new Proxy(un);
		return unit;
	}
}