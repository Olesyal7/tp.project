#pragma once
#include <iostream>
#include "Unit.h"
#include <unordered_set>

using namespace std;

class Observer {
public:
	void get_mana(int val);
	void subscribe(Unit* un);
	void unsubscribe(Unit* un);
	void notify(int val);
private:
	unordered_set<Unit*> buffer;
};
