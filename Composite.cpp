#pragma once
#include "Composite.h"
#include <vector>

using namespace std;

void CompositeUnit::pr() {
	for (int i = 0; i < buffer.size(); ++i)
		buffer[i]->pr();
	cout << endl;
}
void CompositeUnit::addUnit(Unit* p) {
	buffer.push_back(p);
}
CompositeUnit::~CompositeUnit() {
	for (int i = 0; i<buffer.size(); ++i)
		delete buffer[i];
}
bool CompositeUnit::be_attacked(int pow) {
	while (get_health() > 0 && pow > 0) {
		auto i = prev(buffer.end());
		if ((**i).get_health() > pow) {
			(**i).be_attacked(pow);
			pow = 0;
		}
		else {
			pow -= (**i).get_health();
			(**i).be_attacked((**i).get_health());
			if ((**i).get_health() == 0) {
				buffer.pop_back();
			}
		}
	}
	return get_health() > 0;
}
int CompositeUnit::get_power() {
	int sum = 0;
	for (int i = 0; i < buffer.size(); i++) {
		sum += buffer[i]->get_power();
	}
	return sum;
}
int CompositeUnit::get_health() {
	int sum = 0;
	for (int i = 0; i < buffer.size(); i++) {
		sum += buffer[i]->get_health();
	}
	return sum;
}
bool CompositeUnit::get_flag() {
	return 0;
}

void CompositeUnit::inc_health(int val) {
	for (int i = 0; i < buffer.size(); i++) {
		buffer[i]->inc_health(val);
	}
}
