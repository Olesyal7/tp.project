#pragma once
#include <iostream>

using namespace std;

class Unit {
public:
	virtual bool be_attacked(int pow) = 0;
	virtual ~Unit() {};
	virtual int get_power() = 0;
	virtual int get_health() = 0;
	virtual bool get_flag() = 0;
	virtual void pr() = 0;
	virtual void inc_health(int val) = 0;
};


class Strong : public Unit {
public:
	virtual bool be_attacked(int pow) = 0;
	virtual ~Strong() {}
	virtual int get_power() = 0;
	virtual int get_health() = 0;
	virtual void pr() = 0;
	virtual void inc_health(int val) = 0;
};

class Health : public Unit {
public:
	virtual bool be_attacked(int pow) = 0;
	virtual ~Health() {};
	virtual int get_power() = 0;
	virtual int get_health() = 0;
	virtual void pr() = 0;
	virtual void inc_health(int val) = 0;
};

class Magic : public Unit {
public:
	virtual bool be_attacked(int pow) = 0;
	virtual ~Magic() {};
	virtual int get_power() = 0;
	virtual int get_health() = 0;
	virtual bool get_flag() = 0;
	virtual void pr() = 0;
	virtual void inc_health(int val) = 0;
};


class StrongKnight : public Strong {
public:
	int get_power() override;
	int get_health() override;
	bool get_flag() override;
	void pr() override;
	StrongKnight();
	bool be_attacked(int pow) override;
	void inc_health(int val) override;
private:
	int power;
	int health;
};

class HealthKnight : public Health {
public:
	int get_power() override;
	int get_health() override;
	bool get_flag() override;
	void pr() override;
	HealthKnight();
	bool be_attacked(int pow) override;
	void inc_health(int val) override;
private:
	int power;
	int health;
};

class MagicKnight : public Magic {
public:
	int get_power() override;
	int get_health() override;
	bool get_flag() override;
	void pr() override;
	MagicKnight();
	bool be_attacked(int pow) override;
	void inc_health(int val) override;
private:
	bool flag;
	int power;
	int health;
};


class StrongDragon : public Strong {
public:
	int get_power() override;
	int get_health() override;
	bool get_flag() override;
	void pr() override;
	StrongDragon();
	bool be_attacked(int pow) override;
	void inc_health(int val) override;
private:
	int power;
	int health;
};

class HealthDragon : public Health {
public:
	int get_power() override;
	int get_health() override;
	bool get_flag() override;
	void pr() override;
	HealthDragon();
	bool be_attacked(int pow) override;
	void inc_health(int val) override;
private:
	int power;
	int health;
};

class MagicDragon : public Magic {
public:
	int get_power() override;
	int get_health() override;
	bool get_flag() override;
	void pr() override;
	MagicDragon();
	bool be_attacked(int pow) override;
	void inc_health(int val) override;
private:
	bool flag;
	int power;
	int health;
};
